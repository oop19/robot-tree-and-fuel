/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject;

import java.util.Scanner;

/**
 *
 * @author a
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        TableMap map = new TableMap(20,20);
        Robot robot = new Robot(2,2,'x', map, 10);
        Bomb bomb = new Bomb(5,5);
        map.addObj(new Tree(10,10));
        map.addObj(new Tree(9,10));
        map.addObj(new Tree(5,10));
        map.addObj(new Tree(11,10));
        map.addObj(new Tree(10,9));
        map.addObj(new Tree(2,5));
        map.addObj(new Tree(3,4));
        map.addObj(new Fuel(0,5,10));
        map.addObj(new Fuel(15,15,5));
        map.addObj(new Fuel(17,6,5));
        map.setBomb(bomb);
        map.setRobot(robot);
        
        while(true){
           map.showMap(); 
            char direction = inputDirection(n);
            if(direction=='q'){
                printByeBye();
                break;
            }
           robot.walk(direction);
           
        }
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner n) {
        String str = n.next();
        char direction = str.charAt(0);
        return direction;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject;

/**
 *
 * @author a
 */
public class Robot extends Obj {

    private TableMap Map;
    int fuel;

    public Robot(int x, int y, char symbol, TableMap Map, int fuel) {
        super(symbol, x, y);
        this.Map = Map;
        this.fuel = fuel;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                if (walkN()) {
                    return false;
                }
                break;
            case 'S':
            case 's':
                if (walkS()) {
                    return false;
                }
                break;
            case 'E':
            case 'd':
                if (walkE()) {
                    return false;
                }
                break;
            case 'W':
            case 'a':
                if (walkW()) {
                    return false;
                }
                break;
            default:
                return false;
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if (Map.isBomb(x, y)) {
            System.out.println("Found Bomb!!! (" + x + ", " + y + ")");
        }
    }

    private boolean canWalk(int x, int y) {
        return fuel > 0 && Map.inMap(x, y) && !Map.isTree(x, y);
    }

    private void reduceFuel() {
        fuel--;
    }

    private boolean walkW() {
        checkFuel();
        if (canWalk(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    public void checkFuel() {
        int fuel = Map.fillFuel(x, y);
        if(fuel>0){
            this.fuel+=fuel;
        }
    }

    private boolean walkE() {
        checkFuel();
        if (canWalk(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    private boolean walkS() {
        checkFuel();
        if (canWalk(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    private boolean walkN() {
        checkFuel();
        if (canWalk(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    @Override
    public String toString() {
        return "Robot>> " + super.toString() + "(Fuel = " + fuel + ")";
    }
}
